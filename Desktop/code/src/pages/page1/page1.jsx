import "./page1.css";
import SearchIcon from "@mui/icons-material/Search";
import GitHubIcon from "@mui/icons-material/GitHub";
import CloseIcon from "@mui/icons-material/Close";
import Card from "../../components/card";
import Input from '@mui/material/Input';
import InputAdornment from '@mui/material/InputAdornment';
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";

const baseURL = "https://api.github.com/users";
export default function Page1() {
  const [data, setData] = useState(null)
  const[searchText,setSearchText]= useState("");
  useEffect(() => {
    getData();
    async function getData() {
      const response = await fetch(baseURL);
      const data = await response.json();
      setData(data);
    }
  }, []);

  function handleSearchText(e){
      setSearchText(e.target.value)
  }
  return (

   
    <div className="container-wrapper">
      <div className="header">
        <div className="head-top">
          <GitHubIcon className="giticon" /> <p>GitHub Profile Viewer</p>{" "}
        </div>
        <Input
          className="search-box"

          startAdornment={
            <InputAdornment position="start">
              <SearchIcon className="iconSearch" />
            </InputAdornment>
          }
          endAdornment={
            <InputAdornment position="end">
              <CloseIcon className="iconSearch" />
            </InputAdornment>
          }
          placeholder="Seach user"
          value={searchText}
          onChange={handleSearchText}
        />
      </div>

      <div className="container">
        {data && data.filter((val)=>{
          if(searchText==="")
          return val;
          else if(val.login.toLowerCase().includes(searchText.toLowerCase()))
          return val;
        }).map((item, index) => (
          <Link className="link" to={`/page2/${item.login}`}>
            <Card
              key={index}
              name={item.login}
            />
          </Link>
        ))}
      </div>
      <p>Note: more search result should be lazy loaded as user scroll down </p>
    </div>
  );
}
